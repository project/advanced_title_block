
# Advanced Title Block

This module adds a new block type that renders the page title with aditional fields:

- Background color
- Background image
- Subtitle text

Background image and Subtitle text fields could be defined on a per node
basis referencing a field on a node or fixed values per block definition.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/advanced_title_block).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/advanced_title_block).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module currently provides no configuration options.


## Maintainers

- Alberto Siles - [hatuhay](https://www.drupal.org/u/hatuhay)