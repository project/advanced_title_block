<?php

namespace Drupal\advanced_title_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AdvancedTitleBlockForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advanced_title_block_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $default_colors = "#000000;#222222;#444444;#eeeeee;#ffffff";
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('advanced_title_block.settings');

    $form['available_colors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Available colors'),
      '#default_value' => $config->get('advanced_title_block.available_colors') ? $config->get('advanced_title_block.available_colors') : $default_colors,
      '#description' => $this->t("Available colors for the background colors. List of HEX colors separated by ';' #000000;#111111"),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @toDo validate and clean up submissions
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('advanced_title_block.settings');
    $config->set('advanced_title_block.available_colors', $form_state->getValue('available_colors'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'advanced_title_block.settings',
    ];
  }

}