<?php

namespace Drupal\advanced_title_block\Plugin\Block;

use Drupal\node\NodeInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\TitleResolver;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a 'AdvancedTitleBlock' block.
 *
 * @Block(
 *  id = "advanced_title_block",
 *  admin_label = @Translation("Advanced Page Title Block"),
 * )
 */
class AdvancedTitleBlock extends BlockBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The title resolver.
   *
   * @var \Drupal\Core\Controller\TitleResolver
   */
  protected $titleResolver;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new AdvancedTitleBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Controller\TitleResolver $title_resolver
   *   The title resolver.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The configuration object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManager $entity_type_manager, TitleResolver $title_resolver, Request $request, RouteMatchInterface $route_match, ImmutableConfig $config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->titleResolver = $title_resolver;
    $this->request = $request;
    $this->routeMatch = $route_match;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('title_resolver'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_route_match'),
      $container->get('config.factory')->get('advanced_title_block.settings'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#description' => $this->t('Subtitle text'),
      '#default_value' => $this->configuration['text'],
      '#maxlength' => 640,
      '#size' => 64,
      '#weight' => '2',
    ];
    $config = $this->config;
    $default_colors = "#000000;#222222;#444444;#eeeeee;#ffffff";
    $colors_string = $config->get('advanced_title_block.available_colors') ? $config->get('advanced_title_block.available_colors') : $default_colors;
    $options = [];
    foreach (explode(";", $colors_string) as $color) {
      $options[$color] = $color;
    }
    $form['color'] = [
      '#type' => 'select',
      '#title' => $this->t('Background Color'),
      '#description' => $this->t('Customize background colors on <a href=":url">Settings Form</a>', [':url' => '/admin/config/user-interface/advanced-title-block']),
      '#options' => $options,
      '#default_value' => $this->configuration['color'],
      '#required' => TRUE,
      '#weight' => '5',
    ];
    $form['background'] = [
      '#type' => 'details',
      '#title' => t('Default background image'),
      '#open' => TRUE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
    ];
    // @todo destinations from settings form and validate it
    $destination = 'public://background/';
    \Drupal::service('file_system')->prepareDirectory($destination);
    $form['background']['image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Background Image'),
      '#description' => $this->t('Uploaded background image'),
      '#default_value' => $this->configuration['background']['image'],
      '#weight' => '10',
      '#upload_validators' => ['file_validate_extensions' => ['jpg png jpeg'],],
      '#upload_location' => $destination,
      '#required' => FALSE,
    ];
    $form['background']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Background Alt Text'),
      '#description' => $this->t('Background Alt text'),
      '#default_value' => $this->configuration['background']['title'],
      '#maxlength' => 640,
      '#size' => 64,
      '#weight' => '11',
    ];

    // @todo add other entity types.
    $form['entity_image'] = [
      '#type' => 'select',
      '#title' => $this->t('Image from entity'),
      '#options' => $this->getFields(['image', 'entity_reference']),
      '#default_value' => isset($this->configuration['entity_image']) ? $this->configuration['entity_image'] : NULL,
      '#required' => FALSE,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => FALSE,
      '#weight' => '11',
    ];
    $form['entity_text'] = [
      '#type' => 'select',
      '#title' => $this->t('Subtitle text from entity'),
      '#options' => $this->getFields(
        ['text', 'text_long', 'text_with_summary', 'string', 'string_long']
      ),
      '#default_value' => isset($this->configuration['entity_text']) ? $this->configuration['entity_text'] : NULL,
      '#required' => FALSE,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => FALSE,
      '#weight' => '12',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Save image as permanent.
    $background = $form_state->getValue('background');

    if (isset($background['image'])) {
      $image = $background['image'];
    }
    if (isset($image[0])) {
      $file = File::load($image[0]);
      $file->setPermanent();
      $file->save();
    }
    else {
      \Drupal::logger('Advanced Title Block')->error('Failed uploading background image');
    }
    $this->configuration['text'] = $form_state->getValue('text');
    $this->configuration['color'] = $form_state->getValue('color');
    $this->configuration['background'] = $form_state->getValue('background');
    if ($form_state->getValue('entity_image')) {
      $this->configuration['entity_image'] = $form_state->getValue('entity_image');
    }
    if ($form_state->getValue('entity_text')) {
      $this->configuration['entity_text'] = $form_state->getValue('entity_text');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = \Drupal::routeMatch()->getParameter('node');
    $uri = '';
    if (isset($this->configuration['entity_image']) && $node instanceof NodeInterface) {
      $field_name = $this->configuration['entity_image'];
      if ($node->hasField($field_name) && $node->$field_name->entity != null) {
        $type = $field_type = $node->get($field_name)->getFieldDefinition()->getType();
        if ($type == 'image') {
          $image = $node->get($field_name)->getValue();
          $image_title = $image[0]['title'];
          $uri = $node->$field_name->entity->getFileUri();
        }
        else {
          $image = $node->$field_name->entity->get('field_media_image')->getValue();
          $image_title = $image[0]['title'];
          $uri = $node->$field_name->entity->field_media_image->entity->getFileUri();
        }
      }
      else {
        $image_title = $this->configuration['background']['title'];
        $image = $this->configuration['background']['image'];
        if (!empty($image[0])) {
          if ($file = File::load($image[0])) {
            $uri = $file->getFileUri();
          }
        }
      }
    }
    else {
      $image_title = $this->configuration['background']['title'];
      $image = $this->configuration['background']['image'];
      if (!empty($image[0])) {
        if ($file = File::load($image[0])) {
          $uri = $file->getFileUri();
        }
      }
    }
    $text = '';
    if (isset($this->configuration['entity_text']) && $node instanceof NodeInterface) {
      $field_name = $this->configuration['entity_text'];
      if ($node->hasField($field_name)) {
        $text = $node->get($field_name)->getValue()[0]['value'];
      }
      else {
        $text = $this->configuration['text'];
      }
    }
    else {
      $text = $this->configuration['text'];
    }

    $title = '';
    if ($node instanceof NodeInterface) {
      $title = $node->get('title')->value;
    } else {
      if ($route = $this->request->attributes->get(RouteObjectInterface::ROUTE_OBJECT)) {
        $title = $this->titleResolver->getTitle($this->request, $route);
      }
      if ($title instanceof TranslatableMarkup) {
        $title = $title->render();
      }
    }

    $build = [];
    $build['advanced_title_block'] = [
      '#theme' => 'advanced_title_block',
      '#title' => $title,
      '#text' => $text,
      '#color' => $this->configuration['color'],
      '#image' => \Drupal::service('file_url_generator')->generateString($uri),
      '#image_title' => $image_title,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['url.path', 'url.query_args'];
  }

  public function getFields($type, $entity = 'node') {
    if (is_array($type)) {
      $fields = [];
      foreach ($type as $t) {
        $new_fields = $this->entityTypeManager->getStorage('field_storage_config')->loadByProperties([
          'entity_type' => $entity,
          'type' => $t,
          'deleted' => FALSE,
          'status' => 1,
        ]);
        $fields = array_merge($fields, $new_fields);
      }
    }
    else {
      $fields = $this->entityTypeManager->getStorage('field_storage_config')->loadByProperties([
        'entity_type' => $entity,
        'type' => $type,
        'deleted' => FALSE,
        'status' => 1,
      ]);
    }
    $field_names = [];
    foreach ($fields as $field) {
      $field_names[$field->getName()] = $field->getName();
    }
    return $field_names;
  }
}
